
## 0.0.19 [07-11-2024]

Add deprecation notice and metadata

See merge request itentialopensource/pre-built-automations/jira-issue-transition!11

2024-07-11 14:32:12 +0000

---

## 0.0.18 [05-26-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/jira-issue-transition!9

---

## 0.0.17 [06-21-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/jira-issue-transition!8

---

## 0.0.16 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/jira-issue-transition!7

---

## 0.0.15 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/jira-issue-transition!5

---

## 0.0.15 [11-15-2021]

* patch/dsup 1003

See merge request itentialopensource/pre-built-automations/jira-issue-transition!6

---

## 0.0.14 [07-03-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/jira-issue-transition!4

---

## 0.0.13 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/jira-issue-transition!1

---

## 0.0.12 [05-05-2021]

* Update README.md, images/jira_issue_transition_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-transition!8

---

## 0.0.11 [03-23-2021]

* Update README.md, images/jira_issue_transition_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-transition!8

---

## 0.0.10 [03-23-2021]

* Update README.md, images/jira_issue_transition_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-transition!8

---

## 0.0.9 [03-01-2021]

* Update README.md, images/jira_issue_transition_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/jira-issue-transition!8

---

## 0.0.8 [03-01-2021]

* patch/2021-03-01T08-00-30

See merge request itential/sales-engineer/selabprebuilts/jira-issue-transition!7

---

## 0.0.7 [02-26-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/issue-transitionjira!4

---

## 0.0.6 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/issue-transitionjira!3

---

## 0.0.5 [02-25-2021]

* Update README.md, images/TransitionJiraIssueAutoStudio.png,...

See merge request itential/sales-engineer/selabdemos/issue-transitionjira!2

---

## 0.0.4 [02-24-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/issue-transitionjira!1

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/issue-transitionjira!1

---

## 0.0.2 [02-17-2021]

* Bug fixes and performance improvements

See commit b48e3c5

---\n\n
