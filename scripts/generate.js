const path = require('path');
const fs = require('fs');

const files = ['package.json', 'manifest.json', 'README.md'];
let success = true;

const PID = process.argv[2];
const gitlabURL = process.argv[3];
const gitlabBranch = process.argv[4];
if (PID === null || PID == undefined || Number.isNaN(parseInt(PID, 10))) {
  console.error('Please run this script with Gitlab Project ID (integer) as an argument');
  process.exit(2);
}
console.log(`Generating artifact.json file w/ ${PID} as GitLab Project ID`);

/*function fileTypetoType(fileType) {
  const underscoreToDash = fileType.replace(/_/g, '-');
  if (
    fileType === 'ac_agenda_jobs' ||
    fileType === 'mop_templates' ||
    fileType === 'mop_analytic_templates' ||
    fileType === 'templates' ||
    fileType === 'workflows'
  ) {
    const singularize = underscoreToDash.substring(
      0,
      underscoreToDash.length - 1
    );
    return singularize;
  }
  if (fileType === 'catalog_store') {
    return 'service-catalog';
  }
  return underscoreToDash;
}*/
const filePath = __dirname.replace("/scripts", "");
try {
  const [packageJSON, manifest, readme] = files.map(entry => {
    const result = fs.readFileSync(path.join(filePath, `./${entry}`), {
      encoding: 'utf-8'
    });

    if (entry !== 'README.md') {
        const parsedRes = JSON.parse(result);
        if (entry === 'manifest.json' && Object.prototype.hasOwnProperty.call(parsedRes, 'version')) {
          delete parsedRes.version;
        }
      return parsedRes;
    } else {
      return result.replace(/\.\//gm, `${gitlabURL}/-/raw/${gitlabBranch}/`);
    }
  });

  // const result = fs.readdirSync(path.join(__dirname, './bundles'));
  const bundles = [];
  // result.forEach(entry => {
  //   console.log(`   ➡ generating ${entry}(s)`);
  //   const fileNames = fs.readdirSync(path.join(__dirname, `./bundles/${entry}`));
  //   fileNames.forEach(file => {
  //     const data = fs.readFileSync(
  //       path.join(__dirname, `./bundles/${entry}/${file}`),
  //       { encoding: 'utf-8' }
  //     );
  //     try {
  //       bundles.push({ type: fileTypetoType(entry), data: JSON.parse(data) });
  //       console.log(`     ⚪ ${file}   -   ✅`);
  //     } catch(err) {
  //       success = false;
  //       console.log(`     ⚪ ${file}   -   ❌  - ${err}`);
  //     }
  //   });
  // });

  const components = manifest.artifacts;
  components.forEach(file => {
    try {
      const data = fs.readFileSync(
        path.join(filePath, `./${file.location}`),
        { encoding: 'utf-8' }
      );
      bundles.push({ type: file.type, data: JSON.parse(data) });
      console.log(`     ⚪ (${file.type})   -   ${file.location}   -   ✅`);
    } catch (err) {
      success = false;
      console.log(`     ⚪  (${file.type})   -   ${file.location}   -   ❌  - ${err}`);
    }
  });

  const metadata = {
    name: packageJSON.name,
    version: packageJSON.version,
    description: packageJSON.description,
    license: packageJSON.license,
    repository: packageJSON.repository,
    keywords: packageJSON.keywords,
    author: packageJSON.author,
    IAPDependencies: packageJSON.IAPDependencies,
    gitlabId: parseInt(PID, 10),
  };

  if (success) {
    console.log(`\n\nFinished successfully`);
  } else {
    console.log(`\n\nFinished with error(s)`);
    process.exit(1);
  }
  const artifact = { metadata, manifest, bundles, readme };
  fs.writeFileSync(
    path.join(filePath, './artifact.json'),
    JSON.stringify(artifact, null, 2)
  );
} catch (err) {
  console.error(`Failed to generate artifact.json file: ${err}`);
  process.exit(1);
}